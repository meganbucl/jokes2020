var express = require("express");
var app = express();
let cmd = require('node-cmd');

var jokes = ["The word nun is just the letter n doing a cartwheel.",
"A transgender person has a child, they are now transparent.",
"What gender pronouns are you supposed to use for chocolate bars? Her/she",
"I call my horse Mayo, and sometimes Mayo neighs.",
"A few puns make me numb, but math puns make me number.",
"It's good to be kneaded when you are knotty. Massages by Tessa 7206207521.",
"The fact that head and shoulders doesn't have a body wash called 'knees and toes' disappoints me.",
"Ducks have feathers to cover their butt quacks.",
"Butterflies are not what they used to be."];

app.get("/", (req,resp)=>{
  resp.send(jokes);
});

app.get("/random", (req,resp)=>{
  var num = Math.floor(Math.random() * jokes.length);
  resp.send([jokes[num]])
})

const port = process.env.PORT || 3000
app.listen(port, ()=>{
  console.log(`Listening on port ${port}`);
  cmd.run(`open http://localhost:${port}`);
  cmd.run("atom .");
})
